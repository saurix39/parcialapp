-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-06-2020 a las 22:51:34
-- Versión del servidor: 8.0.17
-- Versión de PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `parcialapp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vivienda`
--

CREATE TABLE `vivienda` (
  `idVivienda` int(25) NOT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  `telefono` int(25) DEFAULT NULL,
  `barrio` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `vivienda`
--

INSERT INTO `vivienda` (`idVivienda`, `direccion`, `telefono`, `barrio`) VALUES
(1, 'tv5411255', 3918570, 'candelaria'),
(2, 'tv44b#10cSur', 5889663, 'candelaria'),
(3, 'tv44b#10mNor', 5889663, 'candelaria'),
(4, 'Cll41C#12mNor', 569855632, 'candelaria'),
(5, 'Cll100H#16BSur', 55698562, 'candelaria'),
(6, 'Cll91S#1-21SOr', 5623588, 'candelaria'),
(7, 'dg45c-558cSur', 36552258, 'candelaria'),
(8, 'dg58c-b47-66', 55896523, 'Bosa rincon'),
(9, 'dg58c-b47-25', 64125558, 'Bosa rincon'),
(10, 'dg58c-b47-26', 64125558, 'Bosa rincon'),
(11, 'dg58c-b47-27', 64125558, 'Bosa rincon'),
(12, 'dg58c-b47-28', 64125558, 'Bosa rincon'),
(13, 'dg58c-b47-29', 64125558, 'Bosa rincon'),
(14, 'dg20c-b47-29', 64125558, 'Bosa rincon'),
(15, 'dg21c-b47-29', 64125558, 'Bosa rincon'),
(16, 'dg21c-b47-29', 632589663, 'Bosa rincon');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `vivienda`
--
ALTER TABLE `vivienda`
  ADD PRIMARY KEY (`idVivienda`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `vivienda`
--
ALTER TABLE `vivienda`
  MODIFY `idVivienda` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
