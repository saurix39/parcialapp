<?php
class ViviendaDAO{
    private $idVivienda;
    private $direccion;
    private $telefono;
    private $barrio;

    public function ViviendaDAO($idVivienda="", $direccion="", $telefono="", $barrio=""){
        $this -> idVivienda = $idVivienda;
        $this -> direccion = $direccion;
        $this -> telefono = $telefono;
        $this -> barrio = $barrio;
    }

    public function insertar(){
        return "insert into vivienda (direccion, telefono, barrio) values ('". $this -> direccion ."', '". $this -> telefono ."', '". $this -> barrio ."')";
    }

    public function consultarCantidad(){
        return "select count(idvivienda) from vivienda";
    }

    public function consultarPaginacion($cantidad, $pagina){
        return "select idvivienda, direccion, telefono, barrio
                from vivienda
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

}
?>