<?php
$vivienda = new Vivienda();
$cantidad = 5;
if(isset($_GET["cantidad"])){
    $cantidad = $_GET["cantidad"];
}
$pagina = 1	;
if(isset($_GET["pagina"])){
    $pagina = $_GET["pagina"];
}
$totalRegistros=$vivienda -> consultarCantidad();
$totalPaginas=ceil($totalRegistros/$cantidad);	
$viviendas = $vivienda -> consultarPaginacion($cantidad, $pagina);
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-2">
        </div>
        <div class="col-8">
            <div class="card text-white bg-dark mt-1">
                <div class="card-header text-center"><h2>Consultar viviendas</h2></div>
                <div class="card-body">
                <div class="text-right">Resultados <?php echo (($pagina-1)*$cantidad+1) ?> al <?php echo (($pagina-1)*$cantidad)+count($viviendas) ?> de <?php echo $totalRegistros ?> registros encontrados</div>
                <table class="table table-striped table-dark">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Direccion</th>
                        <th scope="col">Telefono</th>
                        <th scope="col">Barrio</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
						$i=1;
						foreach($viviendas as $viviendaActual){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $viviendaActual -> getDireccion() . "</td>";
						    echo "<td>" . $viviendaActual -> getTelefono() . "</td>";
						    echo "<td>" . $viviendaActual -> getBarrio() . "</td>";
						    echo "</tr>";
						    $i++;
						}
						?>
                    </tbody>
                    </table>
                    <div class="text-center">
        				<nav>
        					<ul class="pagination">
        						<li class="page-item <?php echo ($pagina==1)? "disabled":""; ?>"><a class="page-link" href="index.php?pid= <?php echo base64_encode("Presentacion/Vivienda/consultarViviendas.php"). "&pagina=" . ($pagina-1) ."&cantidad=". $cantidad ?>"> &lt;&lt; </span></a></li>
								<?php for($i=1;$i<=$totalPaginas;$i++){
										if($pagina==$i){
											echo "<li class='page-item active' aria-current='page'><span class='page-link'> ". $i ."<span class='sr-only'></span></span></li>";
										}else{
											echo "<li class='page-item'><a class='page-link' href='index.php?pid=". base64_encode("Presentacion/Vivienda/consultarViviendas.php") ."&pagina=". $i . "&cantidad=". $cantidad ."'>". $i ."</a></li>";
										}
									}	?>
        						<li class="page-item <?php echo ($pagina==$totalPaginas)?"disabled":""; ?>"><a class="page-link" href="<?php echo "index.php?pid=". base64_encode("Presentacion/Vivienda/consultarViviendas.php") ."&pagina=". ($pagina+1) . "&cantidad=". $cantidad?>"> &gt;&gt; </a></li>
								<li>
									<select id="cantidad">
  										<option value="5" <?php echo ($cantidad==5)?"selected":"" ?>>5</option>
  										<option value="10" <?php echo ($cantidad==10)?"selected":"" ?>>10</option>
  										<option value="15" <?php echo ($cantidad==15)?"selected":"" ?>>15</option>
 										<option value="20" <?php echo ($cantidad==20)?"selected":"" ?>>20</option>
									</select>
								</li>	
							</ul>						
        				</nav>
					</div>
                </div>
            </div>
        </div>
    </div>    
</div>
<script>
$("#cantidad").on("change", function(){
	url="index.php?pid=<?php echo base64_encode("Presentacion/Vivienda/consultarViviendas.php")?>&cantidad="+ $(this).val();
	location.replace(url);
});
</script>