<?php
$direccion="";
if(isset($_POST["direccion"])){
	$direccion=$_POST["direccion"];
}
$telefono="";
if(isset($_POST["telefono"])){
	$telefono=$_POST["telefono"];
}
$barrio="";
if(isset($_POST["barrio"])){
	$barrio=$_POST["barrio"];
}
$crear="";
if(isset($_POST["crear"])){
    $vivienda=new Vivienda("", $direccion, $telefono, $barrio);
    $vivienda -> insertar();
}
?>
<div class="container-fluid">
    <div class="row">
		<div class="col-3"></div>
		<div class="col-6">
            <div class="card text-white bg-dark mt-1">
				<div class="card-header">
					<h4>Crear vivienda</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["crear"])){ ?>
						<div class="alert alert-success alert-dismissible fade show" role="alert">
							Datos ingresados
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						</div>
			      	<?php } ?>
              		<form action="index.php?pid=<?php echo base64_encode("Presentacion/Vivienda/crearVivienda.php")?>" method="post">
						<div class="form-group">
							<label>Direccion</label> 
							<input type="text" name="direccion" value ="<?php echo $direccion?>" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Telefono</label> 
							<input type="number" name="telefono" value ="<?php echo $telefono?>" class="form-control" min="1" required>
						</div>
						<div class="form-group">
							<label>Barrio</label> 
							<input type="text" name="barrio" value ="<?php echo $barrio?>" class="form-control" required>
						</div>
                        
						<button type="submit" name="crear" class="btn btn-secondary form-control">Crear</button>
					</form>
            	</div>
            </div>
		</div>
	</div>

</div>