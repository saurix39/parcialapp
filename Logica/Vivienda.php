<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/ViviendaDAO.php";
class Vivienda{
    private $idVivienda;
    private $direccion;
    private $telefono;
    private $barrio;
    private $conexion;
    private $viviendaDAO;

    public function Vivienda($idVivienda="", $direccion="", $telefono="", $barrio=""){
        $this -> idVivienda = $idVivienda;
        $this -> direccion = $direccion;
        $this -> telefono = $telefono;
        $this -> barrio = $barrio;
        $this -> conexion = new Conexion();
        $this -> viviendaDAO = new ViviendaDAO($this -> idVivienda, $this -> direccion, $this -> telefono, $this -> barrio);
    }

    public function insertar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> viviendaDAO -> insertar());
        $this -> conexion -> cerrar();
    }

    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> viviendaDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this-> conexion -> extraer()[0];
    }

    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> viviendaDAO -> consultarPaginacion($cantidad, $pagina));
        $viviendas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $v = new Vivienda($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($viviendas, $v);
        }
        $this -> conexion -> cerrar();
        return $viviendas;
    }

    public function getIdVivienda()
    {
        return $this->idVivienda;
    }

    public function getDireccion()
    {
        return $this->direccion;
    }

    public function getTelefono()
    {
        return $this->telefono;
    }

    public function getBarrio()
    {
        return $this->barrio;
    }

    public function getConexion()
    {
        return $this->conexion;
    }

    public function getViviendaDAO()
    {
        return $this->viviendaDAO;
    }
}
?>