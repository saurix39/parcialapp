<?php
require_once "Persistencia/ViviendaDAO.php";
require_once "Logica/Vivienda.php";

$pid = "";
if(isset($_GET["pid"])){
    $pid = base64_decode($_GET["pid"]);    
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="img/Casa.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <title>ParcialApp</title>
</head>
<body>
<div class="p-3 mb-2 bg-secondary">
    <?php
        include "Presentacion/encabezado.php";
        include "Presentacion/menu.php";
        if($pid!=""){
            include $pid;
        }else{
            include "Presentacion/inicio.php";
        }
        
    ?>
</div>
</body>
</html>